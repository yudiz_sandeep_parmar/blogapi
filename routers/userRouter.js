const express = require('express');
const router = new express.Router();
const auth = require('../middleware/auth');
const { validators } = require('../validator/bodyValidator');

const userController = require('../controllers/userController');
const multer = require('multer');
const upload = multer({ dest: 'uploads' });

router.post(
  '/users/reg',
  upload.single('sProfileImage'),
  validators.fileCheck,
  validators.reg,
  userController.register
);

router.post('/users/login', userController.login);

router.patch(
  '/users/edit/',
  auth.userAuthorization,
  upload.single('sProfileImage'),
  validators.fileCheck,

  validators.editProfile,
  userController.editProfile
);
// router.post(
//   '/users/sendResetLink',

//   userController.resetLink
// );

router.patch(
  '/users/resetPassword/:token',

  userController.changePassword
);
router.get('/users/list', auth.userAuthorization, auth.adminAuthorization, userController.userList);
router.get('/users/profile', auth.userAuthorization, userController.userProfile);

router.get('/users/logout', auth.userAuthorization, userController.logout);
router.get('/users/logoutALL', auth.userAuthorization, userController.logoutAll);
router.delete('/users/delete/:id', auth.userAuthorization, auth.adminAuthorization, userController.delete);

// router.get(
//   '/admin',
//   auth.userAuthorization,
//   auth.adminAuthorization('ADMIN'),
//   userController.userProfile
// );
module.exports = router;
