const mongoose = require('mongoose');

const bookSchema = new mongoose.Schema(
  {
    oUserId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'users'
    }
  },
  {
    timestamps: true
  }

);
bookSchema.virtual('textbook', {
  ref: 'textbooks',
  localField: '_id',
  foreignField: 'iBookId'
});
bookSchema.virtual('coloringbook', {
  ref: 'coloringbooks',
  localField: '_id',
  foreignField: 'iBookId'
});

const bookModel = mongoose.model('books', bookSchema);

module.exports = bookModel;
