const serverController = {};

serverController.server = async (req, res) => {
  try {
    return res.json({ response: 'Welcome to blog!' });
  } catch (error) {
    return res.status(404).json({ message: error.message });
  }
};

serverController.wildCard = async (req, res) => {
  try {
    return res.json({ message: 'Route not found!' });
  } catch (error) {
    return res.status(404).json({ message: error.message });
  }
};

module.exports = serverController;
