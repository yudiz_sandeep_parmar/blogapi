const { ApolloError } = require('apollo-server-errors');

class MyError extends ApolloError {
  constructor(message) {
    super(message, '404');

    Object.defineProperty(this, 'name', { value: 'Custom404' });
  }
}
module.exports = MyError;
