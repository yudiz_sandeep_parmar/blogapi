const { GraphQLScalarType, Kind } = require('graphql');
const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

const emailScalar = new GraphQLScalarType({
  name: 'Email',
  description: 'Email custom scalar type',
  serialize(value) {
    // console.log(value);
    return value;
  },
  parseValue(value) {
    console.log(regex.test(value));
    if (!regex.test(value)) {
      throw new Error(`${value} is not a valid email`);
    }
    return value;
  },
  parseLiteral(ast) {
    if (ast.kind !== Kind.STRING) {
      throw new Error('Query error: Can only parse strings got a: ' + ast.kind);
    }
    if (!regex.test(ast.value)) {
      throw new Error(`${ast.value} is not a valid email`);
    }
    return ast.value;
  }
});

module.exports = emailScalar;
