const typeDefs = require('./lib/typeDefs.js');
const resolvers = require('./lib/resolvers.js');
const directive = require('./lib/directive.js');
const context = require('./lib/context.js');
const blog = require('./datasources/Blog.js');
const user = require('./datasources/User.js');
// const shield = require('./middleware/');
const errors = require('./lib/customError');
module.exports = {
  typeDefs,
  resolvers,
  directive,
  context,
  blog,
  user,
  errors
};
