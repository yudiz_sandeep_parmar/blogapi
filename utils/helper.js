const jwt = require('jsonwebtoken');

const _ = {};

_.verifyToken = function(token) {
  try {
    return jwt.verify(token, process.env.TOKEN, function(err, decoded) {
      return err ? err.message : decoded; // return true if token expired
    });
  } catch (error) {
    return error ? error.message : error;
  }
};

module.exports = _;
