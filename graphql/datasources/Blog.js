const { RESTDataSource } = require('apollo-datasource-rest');

class Blog extends RESTDataSource {
  constructor() {
    super();
    this.baseURL = process.env.BASE_URL;
  }

  willSendRequest(request) {
    request.headers.set('Authorization', this.context.token);
  }

  getBlogs() {
    try {
      return this.get('publishedBlogs');
    } catch (error) {
      console.log(error);
      return error;
    }
  }
}
module.exports = new Blog();
