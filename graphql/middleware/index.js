const { shield, not, rule } = require('graphql-shield');

const isAuthenticated = rule({ cache: 'contextual' })(
  async(parent, args, ctx, info) => {
    return ctx.user !== null;
  }
);

module.exports.permissions = shield({
  Query: {
    users: not(isAuthenticated)
  }
});
