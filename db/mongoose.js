const mongoose = require('mongoose');

const url = process.env.MONGO_URL;
const connectDB = async() => {
  try {
    await mongoose.connect(url);
    console.log('connected to db...');
  } catch (error) {
    console.log(error.message);

    // exit process with failure
    process.exit(1);
  }
};

module.exports = connectDB;
