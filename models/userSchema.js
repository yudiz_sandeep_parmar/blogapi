const mongoose = require('mongoose');
const jwt = require('jsonwebtoken');

const bcryptjs = require('bcryptjs');
// const blogModel = require('./blogSchema');
const {
  validateEmail,
  validatePhone
} = require('../validator/schemaValidator');

const userSchema = new mongoose.Schema(
  {
    sUserName: {
      type: String,
      required: true,
      unique: true,
      trim: true
    },
    sEmail: {
      type: String,
      required: true,
      trim: true,
      lowercase: true,
      validate: [validateEmail, 'Invalid Email!'],
      unique: true
    },
    sPassword: {
      type: String,
      required: true,
      trim: true,
      minlength: 6
    },
    sRole: [
      {
        type: String,
        enum: ['USER', 'ADMIN']
      }
    ],
    aTokens: [
      {
        token: {
          type: String,
          required: true
        }
      }
    ],
    sProfileImage: {
      type: String
    },
    sMobile: {
      type: String,
      required: true,
      validate: [validatePhone, 'Invalid mobile Number!']
    },
    sGender: {
      type: String,
      enum: ['Male', 'Female', 'Other']
    }
  },
  {
    timestamps: true
  }
);

userSchema.virtual('blogs', {
  ref: 'blogs',
  localField: '_id',
  foreignField: 'oUserId'
});

userSchema.virtual('userByBooks', {
  ref: 'books',
  localField: '_id',
  foreignField: 'oUserId'
});

userSchema.statics.findByCredentials = async(email, password) => {
  const user = await userModel.findOne({ sEmail: email });
  if (!user) {
    throw new Error('Unable to login!');
  }
  const isMatch = await bcryptjs.compare(password, user.sPassword);

  if (!isMatch) {
    throw new Error('Unable to login!');
  }
  return user;
};

userSchema.methods.generateAuthToken = async function() {
  const user = this;
  const token = jwt.sign({ _id: user.id.toString() }, process.env.TOKEN);

  user.aTokens.push({ token });
  await user.save();

  return token;
};

userSchema.pre('save', async function(next) {
  const user = this;
  if (user.isModified('sPassword')) {
    user.sPassword = await bcryptjs.hash(user.sPassword, 8);
  }
  next();
});

userSchema.pre('save', async function(next) {
  if (this.sRole.indexOf('USER') === -1) this.sRole.push('USER');
  next();
});

// userSchema.pre('deleteOne', async function (next) {
// await blogModel.deleteMany({ oUserId: this._conditions._id });
// next();
// });

const userModel = mongoose.model('users', userSchema);

module.exports = userModel;
