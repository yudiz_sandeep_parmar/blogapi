require('../env');
const cors = require('cors');
const express = require('express');
const bodyParser = require('body-parser');
const { ApolloServer } = require('apollo-server-express');
const { makeExecutableSchema } = require('@graphql-tools/schema');
const { typeDefs, resolvers, directive, context, blog, user } = require('../graphql/');
const router = require('../routers/router');
const connectDB = require('../db/mongoose');
const app = express();

let schema = makeExecutableSchema({
  typeDefs,
  resolvers
});
schema = directive(schema, 'upper');
async function init() {
  connectDB();

  app.use(express.json());
  app.use(bodyParser.urlencoded({ extended: true }));
  app.use(cors({ origin: '*', exposedHeaders: ['Content-Range'] }));
  const server = new ApolloServer({
    schema,
    context,
    dataSources: () => {
      return {
        userSource: user,
        blogSource: blog
      };
    },
    csrfPrevention: true,
    cache: 'bounded'
  });
  await server.start();
  server.applyMiddleware({ app });
  app.use(router);
  app.listen(process.env.PORT || 3000, () => {
    console.log('🚀 Server started at port: ' + process.env.PORT, `${server.graphqlPath}`);
  });
}

module.exports = init;
