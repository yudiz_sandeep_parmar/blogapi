// const BlogModel = require('../../models/blogSchema');
// const UserModel = require('../../models/userSchema');
const EmailResolver = require('./scalars');
const resolvers = {
  Email: EmailResolver,
  blogUnion: {
    __resolveType(obj, context, info) {
      if (obj.sUserName) {
        return 'Blog';
      }
      if (obj.sTitle) {
        return 'Blog';
      }
      return 1; // GraphQLError is thrown
    }
  },
  Book: {
    __resolveType(obj, context, info) {
      if (obj.sCourse) {
        return 'Textbook';
      }
      if (obj.sColor) {
        return 'ColoringBook';
      }
      return 1; // GraphQLError is thrown
    }
  },
  Query: {
    blogs: async(_, __, { dataSources }) => {
      return dataSources.blogSource.getBlogs();
    },
    users: async(_, __, { dataSources }) => dataSources.userSource.getUsers(),
    search: async(root, { args }, context) => {
      return BlogModel.find({ $or: [{ sUserName: args }, { sTitle: { $regex: args, $options: 'i' } }] });
    },
    books: async(root, { args }, context) => {
      const data = await TextBookModel.find({ sCourse: args }).count();

      if (data) {
        return TextBookModel.find({ sCourse: args }).populate('iBookId');
      }
      return ColorBookModel.find({ sColor: args }).populate('iBookId');
    },
    searchUser: async(root, { args }, context) => {
      return UserModel.find({ $or: [{ sUserName: args }, { sEmail: args }] });
    }

  }
  // Role: { ADMIN: 'ADMIN', USER: 'USER' }

};
module.exports = resolvers;
