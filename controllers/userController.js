const UserModel = require('../models/userSchema');
const userController = {};
// const { comparePassword } = require('../utils/hash');

const { uploadFile, deleteFile, deleteFiles } = require('../utils/upload');
// const sendEmail = require("../utils/mail");
const jwt = require('jsonwebtoken');
// const { default: mongoose } = require("mongoose");
const blogModel = require('../models/blogSchema');

userController.register = async(req, res) => {
  try {
    let uploadData;
    if (req.file) {
      uploadData = await uploadFile(req.file);
    }
    const body = {
      sUserName: req.body.sUserName,
      sEmail: req.body.sEmail,
      sPassword: req.body.sPassword,
      sMobile: req.body.sMobile,
      sGender: req.body.sGender,
      sRole: req.body.sRole || 'USER',
      sProfileImage: uploadData?.Location || ''
    };

    const userData = new UserModel(body);
    const token = await userData.generateAuthToken();

    return res.status(201).json({ message: 'successfully registered', userData, token });
  } catch (error) {
    return res.status(400).json({ message: error.message });
  }
};

userController.login = async(req, res) => {
  try {
    const user = await UserModel.findByCredentials(req.body.sEmail, req.body.sPassword);
    if (user.aTokens.length > 5) user.aTokens.shift();
    const token = await user.generateAuthToken();

    return res.status(201).json({ message: 'Logged in!', token });
  } catch (e) {
    return res.status(400).json(e.message);
  }
};

userController.editProfile = async(req, res) => {
  try {
    let uploadData;
    if (req.file) {
      uploadData = await uploadFile(req.file);
    }
    if (req.user.sProfileImage) {
      if (req.user.sProfileImage !== uploadData?.Location) {
        await deleteFile(req.user?.sProfileImage);
      }
    }
    const body = {
      sEmail: req.body.sEmail || req.user.sEmail,
      sMobile: req.body.sMobile || req.user.sMobile,
      sProfileImage: uploadData?.Location || req.user.sProfileImage
    };
    await UserModel.updateOne({ _id: req.user.id }, body);
    return res.status(200).json({ message: 'Data updated' });
  } catch (error) {
    console.log(error);
    res.status(400).json({
      error: error
    });
  }
};

// userController.resetLink = async(req, res) => {
//   try {
//     const { sEmail } = req.body;
//     const token = await jwt.sign({ _id: sEmail }, process.env.TOKEN, {
//       expiresIn: '12h'
//     });
//     const link = `${req.protocol}://${req.get('host')}/users/resetPassword/${token}`;

//     await sendEmail(
//       sEmail,
//       'sparmar586@rku.ac.in',
//       'Blog Password Reset',`
//        <div>Click the link below to reset your password</div><br/>
//         <div><a href=${link}>Link</a></div>
//         `
//     );

//     return res.status(200).send({ message: 'Password reset link has been successfully sent' });
//   } catch (e) {
//     return res.status(400).json({ message: e.message });
//   }
// };

userController.changePassword = async(req, res) => {
  try {
    const { sPassword } = req.body;
    const { token } = req.params;
    const decoded = jwt.verify(token, process.env.token);
    const user = await UserModel.findOne({ sEmail: decoded._id });
    user.sPassword = sPassword;
    await user.save();

    return res.status(200).json({ message: 'Password Updated' });
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};

userController.userProfile = async(req, res) => {
  try {
    return res.send(req.user);
  } catch (e) {
    return res.status(400).json({ message: e.message });
  }
};

userController.logout = async(req, res) => {
  try {
    req.user.aTokens = req.user.aTokens.filter((token) => {
      return token.token !== req.token;
    });
    await req.user.save();
    return res.json({ message: 'Logout Successfully!' });
  } catch (e) {
    return res.status(500).json({ message: e.message });
  }
};

userController.logoutAll = async(req, res) => {
  try {
    req.user.aTokens = [];
    await req.user.save();
    return res.json({ message: 'Logout successfully on all devices!' });
  } catch (e) {
    return res.status(500).json({ message: e.message });
  }
};

userController.delete = async(req, res) => {
  try {
    const blogArray = await blogModel.find({ oUserId: req.params.id }, { _id: 0, Key: '$sCoverImage' }).lean();

    const deleteUser = await UserModel.deleteOne({ _id: req.params.id });
    if (deleteUser.deletedCount) {
      await blogModel.deleteMany({ oUserId: req.params.id });
      deleteFiles(blogArray);
      return res.status(200).json({ message: 'Deleted User with corresponding blogs' });
    }
    return res.status(200).json({ message: 'User not found' });
  } catch (e) {
    return res.status(500).json({ message: e.message });
  }
};

userController.userList = async(req, res) => {
  try {
    const search = new RegExp(req.query.sUserName);
    const limit = 5;
    const page = req.query.pageNumber || 1;
    if (req.query.sUserName) {
      const user = await UserModel
        .find({
          sUserName: { $regex: search, $options: 'i' }
        })
        .skip(page * limit - limit)
        .limit(limit);
      if (!user.length) {
        return res.status(200).json({ message: 'No User Found!' });
      }
      return res.status(200).json({ user });
    }
    const user = await UserModel
      .find({})
      .skip(page * limit - limit)
      .limit(limit);
    if (!user.length) {
      return res.status(200).json({ message: 'No User Found!' });
    }
    // if (req.query.) {

    // }
    return res.status(200).json({ user });
  } catch (e) {
    return res.status(500).json({ message: e.message });
  }
};

module.exports = userController;
