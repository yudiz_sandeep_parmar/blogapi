const fs = require("fs");

const AWS = require("aws-sdk");
const util = require("util");
const unlinkFile = util.promisify(fs.unlink);
const s3 = new AWS.S3({
	accessKeyId: process.env.AWS_ACCESS_KEY,
	secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
	region: process.env.S3_region,
});
async function uploadFile(file) {
	try {
		const type = file.originalname.split(".")[1];
		const fileStream = fs.createReadStream(file.path);
		const uploadParams = {
			Bucket: process.env.BUCKET,
			Body: fileStream,
			Key: `${file.filename}.${type}`,
		};

		const response = await s3.upload(uploadParams).promise();

		await unlinkFile(file.path);

		return response;
	} catch (error) {
		await unlinkFile(file.path);
		return error.message;
	}
}
async function deleteFile(file) {
	const fileName = file.split("/")[3];
	const params = {
		Bucket: process.env.BUCKET,
		Key: fileName,
	};
	const response = await s3.deleteObject(params).promise();
	return response;
}
async function deleteFiles(array) {
	const data = array.map((file) => {
		const Key = file.Key.split("/")[3];
		return { Key };
	});
	const params = {
		Bucket: process.env.BUCKET,
		Delete: { Objects: data },
	};
	const response = await s3.deleteObjects(params).promise();
	return response;
}

module.exports = { uploadFile, deleteFile, deleteFiles };
