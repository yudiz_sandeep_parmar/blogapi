const { MongoDataSource } = require('apollo-datasource-mongodb');
const UserModel = require('../../model-routes-services/user/model');
const Custom404 = require('../lib/customError');

class Users extends MongoDataSource {
  constructor() {
    super(UserModel);
    this.userModel = UserModel;
  }

  async getUsers() {
    try {
      if (!this.context.token) {
        throw new Error('Not authorized!');
      }
      const data = await this.userModel.find({});
      if (!data) {
        throw new Custom404('Not Found!!');
      }
      return data;
    } catch (error) {
      console.log(error);
      return error;
    }
  }
}

module.exports = new Users();
