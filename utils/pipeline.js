const blogsPipeline = (query) => {
	let pipeline = [];
	const order = !query.order ? -1 : parseInt(query.order);
	const limit = parseInt(query.limit || 5);
	const page = query.page || 1;
	const skip = page * limit - limit;
	const date = new Date();

	if (query.published) {
		pipeline.push({
			$match: {
				isPublished: query.isPublished == "true" ? true : false,
			},
		});
	}

	if (query.search) {
		const regex = new RegExp(query.keyword);
		pipeline.push({ $match: { [query.search]: { $regex: regex, $options: "i" } } });
	}

	if (query.startDate) {
		const startDate = new Date(query.startDate);
		const endDate = new Date(query.endDate);
		pipeline.push({ $match: { [query.dateField]: { $gte: startDate, $lte: endDate } } });
	}

	if (query.days) {
		let searchDate = new Date();
		searchDate.setDate(searchDate.getDate() + parseInt(query.days));
		pipeline.push({ $match: { dPublishedDate: { $gte: searchDate, $lte: date } } });
	}

	pipeline.push(
		{ $set: { nLike: { $size: "$aLikes" } } },
		{ $sort: { nLike: order } },
		{
			$facet: {
				data: [{ $skip: skip }, { $limit: limit }],
				metaData: [{ $count: "count" }, { $addFields: { page: { $ceil: { $divide: ["$count", limit] } } } }],
			},
		}
	);
	return pipeline;
};

module.exports = blogsPipeline;
