// const UserModel = require('../../models/userSchema');
const { _ } = require('../../utils/');
const context = async({ req }) => {
  try {
  // get the user token from the headers
    const token = req.headers.authorization;
    if (!token) {
      throw new Error('Header not found!');
    }
    // try to retrieve a user with the token

    const decoded = _.verifyToken(token);
    const user = await UserModel.findOne({
      _id: decoded._id,
      'aTokens.token': token
    });

    if (!user) {
      throw new Error('Token expired!');
    }

    req.token = token;
    req.user = user;

    // add the user to the context
    return { user: req.user, token: req.token };
  } catch (e) {
    console.log(e);
    throw new Error('Please Authenticate!');
  }
};

module.exports = context;
