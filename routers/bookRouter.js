const express = require('express');
const router = new express.Router();

const auth = require('../middleware/auth');
const bookController = require('../controllers/bookController');

router.post(
  '/createBooks',
  auth.userAuthorization,
  bookController.create
);
router.get('/getBooks', auth.userAuthorization, bookController.getBooks);

module.exports = router;
