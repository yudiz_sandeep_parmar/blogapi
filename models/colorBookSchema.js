const mongoose = require('mongoose');

const coloringBookSchema = new mongoose.Schema(
  {
    iBookId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'books'
    },
    sTitle: {
      type: String
    },
    sColor: {
      type: String,
      trim: true,
      required: true
    }
  },
  {
    timestamps: true
  }
);

const ColorBookModel = mongoose.model('coloringBooks', coloringBookSchema, 'coloringBooks');

module.exports = ColorBookModel;
