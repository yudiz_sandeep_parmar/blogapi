const express = require('express');
const router = new express.Router();
const { validators } = require('../validator/bodyValidator');

const auth = require('../middleware/auth');
const blogController = require('../controllers/blogController');
const multer = require('multer');
const upload = multer({ dest: 'uploads' });

router.post(
  '/createBlog',
  auth.userAuthorization,
  upload.single('sCoverImage'),
  validators.fileCheck,
  blogController.create
);
router.get('/publish/:id', auth.userAuthorization, blogController.publish);
router.get('/like/:id', auth.userAuthorization, blogController.like);
router.get('/archive/:id', auth.userAuthorization, blogController.archive);
router.get('/publishedBlogs', auth.userAuthorization, blogController.publishedBlogs);

router.get('/userBlog', auth.userAuthorization, blogController.getUserBlogs);
router.get('/search', auth.userAuthorization, auth.adminAuthorization, blogController.search);

module.exports = router;
