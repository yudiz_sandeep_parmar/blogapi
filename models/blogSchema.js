const mongoose = require('mongoose');

const blogSchema = new mongoose.Schema(
  {
    oUserId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'users'
    },
    sUserName: {
      type: String,
      required: true
    },
    sTitle: {
      type: String,
      trim: true,
      required: true
    },
    sDescription: {
      type: String,
      required: true
    },
    dPublishedDate: {
      type: Date
    },
    sCoverImage: {
      type: String
    },
    isPublished: {
      type: Boolean,
      default: false
    },
    aLikes: [{ type: mongoose.Schema.Types.ObjectId, required: true }]
  },
  {
    timestamps: true
  }
);
blogSchema.index({ sUserName: 1, sTitle: 1 }, { unique: true });
const blogModel = mongoose.model('blogs', blogSchema);

module.exports = blogModel;
