const bcryptjs = require('bcryptjs');

const hashPassword = async (password) => {
  return await bcryptjs.hash(password, 5);
};

const comparePassword = async (password, hashPassword) => {
  return await bcryptjs.compare(password, hashPassword);
};

module.exports = { hashPassword, comparePassword };
