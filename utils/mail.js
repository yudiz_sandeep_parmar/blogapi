const sgMail = require("@sendgrid/mail");

sgMail.setApiKey(
	process.env.SENDGRID_KEY ||
		"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"
);
const sendEmail = (receiver, source, subject, content) => {
	try {
		const data = {
			to: receiver,
			from: source,
			subject,
			html: content,
		};
		return sgMail.send(data);
	} catch (e) {
		return new Error(e.message);
	}
};

module.exports = sendEmail;
