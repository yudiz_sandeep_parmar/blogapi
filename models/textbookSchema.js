const mongoose = require('mongoose');

const textbookSchema = new mongoose.Schema(
  {
    iBookId: {
      type: mongoose.Schema.Types.ObjectId,
      required: true,
      ref: 'books'
    },
    sTitle: {
      type: String
    },
    sCourse: {
      type: String,
      trim: true,
      required: true
    }
  },
  {
    timestamps: true
  }
);

const TextBookModel = mongoose.model('textBooks', textbookSchema, 'textBooks');

module.exports = TextBookModel;
