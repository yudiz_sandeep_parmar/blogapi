const BlogModel = require('../models/blogSchema');
const { uploadFile } = require('../utils/upload');

const blogsPipeline = require('../utils/pipeline');
const blogController = {};

blogController.create = async(req, res) => {
  try {
    let uploadData;
    const { id, sUserName } = req.user;
    if (req.file) {
      uploadData = await uploadFile(req.file);
    }

    const body = new BlogModel({
      oUserId: id,
      sUserName,
      sTitle: req.body.sTitle,
      sDescription: req.body.sDescription,
      sCoverImage: uploadData?.Location || ''
    });
    await body.save();
    return res.status(201).json({ message: 'blog created ' });
  } catch (e) {
    return res.status(500).json({ message: e.message });
  }
};

blogController.getUserBlogs = async(req, res) => {
  try {
    const limit = 5;
    const page = req.query.pageNumber || 1;
    await req.user.populate({
      path: 'blogs',
      select: '_id, oUserId, sUserName, sTitle, sDescription, sCoverImage, isPublished, aLikes, dPublishedDate',
      options: { skip: page * limit - limit, limit: limit }
    });
    return res.status(200).json(req.user.blogs);
  } catch (e) {
    return res.status(500).json({ message: e.message });
  }
};

blogController.publishedBlogs = async(req, res) => {
  try {
    const limit = 5;
    const page = req.query.pageNumber || 1;
    const blog = await BlogModel
      .find({ isPublished: true }, { _id: 0 })
      .skip(page * limit - limit)
      .limit(limit);
    return res.status(200).json(blog);
  } catch (e) {
    return res.status(500).json({ message: e.message });
  }
};

blogController.publish = async(req, res) => {
  try {
    const findId = await BlogModel.find({
      oUserId: req.user.id,
      _id: req.params.id
    });
    if (findId.length) {
      if (findId[0].isPublished === false) {
        await BlogModel.updateOne({ _id: req.params.id }, { dPublishedDate: new Date(), isPublished: true });
        return res.status(200).json({ message: 'Blog Published!' });
      }
      return res.status(200).json({ message: 'Blog is already published!' });
    }
    return res.status(401).json({ message: 'Not your blog!' });
  } catch (e) {
    return res.status(500).json({ message: e.message });
  }
};

blogController.search = async(req, res) => {
  try {
    const query = blogsPipeline(req.query);
    const blog = await BlogModel.aggregate(query);
    if (!blog) {
      return res.status(500).json({ message: 'Not Found!' });
    }
    return res.status(200).json({ blog });
  } catch (e) {
    return res.status(500).json({ message: e.message });
  }
};

blogController.archive = async(req, res) => {
  try {
    const { id } = req.params;
    const findId = await BlogModel.find({ oUserId: req.user.id, _id: id });
    if (findId.length) {
      if (findId[0].isPublished === true) {
        await BlogModel.updateOne({ _id: id }, { isPublished: false, $unset: { dPublishedDate: 1 } });
        return res.status(200).json({ message: 'Archived!' });
      }
      return res.status(200).json({ message: 'Blog is already archived!' });
    }
    return res.status(401).json({ message: 'Not your blog!' });
  } catch (e) {
    return res.status(500).json({ message: e.message });
  }
};

blogController.like = async(req, res) => {
  try {
    const { id } = req.params;
    const { _id } = req.user;
    const blog = await BlogModel.findById({ _id: id });
    const isLike = blog.aLikes.includes(_id);
    if (!isLike) {
      blog.aLikes.addToSet(_id);
      const response = await blog.save();
      if (!response) return res.status(404).json({ message: 'Something went wrong!' });
      return res.status(201).json({ message: 'like added' });
    }
    blog.aLikes.pull(_id);
    const response = await blog.save();
    if (!response) return res.status(404).json({ message: 'Something went wrong!' });
    return res.status(201).json({ message: 'like removed' });
  } catch (e) {
    return res.status(500).json({ message: e.message });
  }
};

module.exports = blogController;
