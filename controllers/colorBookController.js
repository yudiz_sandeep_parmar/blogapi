const ColorBookModel = require('../models/colorBookSchema');
const colorBookController = {};

colorBookController.create = async(req, res) => {
  try {
    ColorBookModel.create({
      iBookId: req.body.bookId,
      sTitle: req.body.sTitle,
      sColor: req.body.sColor

    }, function(err) {
      if (err) return err;
      return res.status(201).json({ message: 'color book created ' });
    });
  } catch (e) {
    return res.status(500).json({ message: e.message });
  }
};
module.exports = colorBookController;
