const fs = require('fs');
const util = require('util');
const unlinkFile = util.promisify(fs.unlink);
const validators = {};
const userModel = require('../models/userSchema');
const { validateEmail, validatePhone } = require('./schemaValidator');
validators.fileCheck = async (req, res, next) =>
{
	if (req.file)
	{
		if (
			!(
				req.file.mimetype === 'image/jpeg' ||
				req.file.mimetype === 'image/jpg' ||
				req.file.mimetype === 'image/png' ||
				req.file.mimetype === 'image/gif'
			)
		)
		{
			await unlinkFile(req.file.path);
			return res
				.status(500)
				.json({ message: 'image format should be jpeg jpg or png' });
		}
	}
	next();
};

validators.reg = async (req, res, next) =>
{
	try
	{
		if (
			req.body.sEmail &&
			req.body.sUserName &&
			req.body.sMobile &&
			req.body.sPassword
		)
		{
			if (!validateEmail(req.body.sEmail))
			{
				throw new Error('Invalid Email');
			}
			if (!validatePhone(req.body.sMobile))
			{
				throw new Error('Invalid mobile number');
			}
			if (req.body.sPassword.length < 6)
			{
				throw new Error('password length should be 6');
			}
			const user = await userModel.find({
				$or: [{ sEmail: req.body.sEmail }, { sUserName: req.body.sUserName }],
			});

			if (user.length)
			{
				throw new Error('Email/UserName Already Used!');
			}
		} else
		{
			throw new Error('email, userName, mobile & password field are required');
		}

		next();
	} catch (e)
	{
		if (req.file)
		{
			await unlinkFile(req.file.path);
		}
		return res.status(400).json({ message: e.message });
	}
};

validators.reset = async (req, res) =>
{
	if (req.body.sEmail)
	{
		if (!validateEmail(req.body.sEmail))
		{
			return res.status(400).json({
				message: 'Invalid Email',
			});
		}

		const user = await userModel.findOne({ sEmail: req.body.sEmail });
		if (user)
		{
			return res.status(201).json({ message: 'Email Already Registered!' });
		}
	} else
	{
		return res.status(400).json({
			message: 'email field is required',
		});
	}

	next();
};

validators.editProfile = async (req, res, next) =>
{
	if (req.body.sEmail && req.body.sMobile)
	{
		if (!validateEmail(req.body.sEmail))
		{
			return res.status(400).json({
				message: 'Invalid Email',
			});
		}
		if (req.user.sEmail != req.body.sEmail)
		{
			const user = await userModel.findOne({
				sEmail: req.body.sEmail,
			});
			if (user)
			{
				return res.status(201).json({ message: 'Email Already Used!' });
			}
		}

		if (req.body.sMobile)
		{
			if (!validatePhone(req.body.sMobile))
			{
				return res.status(400).json({
					message: 'Invalid mobile number',
				});
			}
		}
	}
	if (req.body.sEmail)
	{
		if (!validateEmail(req.body.sEmail))
		{
			return res.status(400).json({
				message: 'Invalid Email',
			});
		}
		if (req.user.sEmail != req.body.sEmail)
		{
			const user = await userModel.findOne({
				sEmail: req.body.sEmail,
			});
			if (user)
			{
				return res.status(201).json({ message: 'Email Already Used!' });
			}
		}

	}
	if (req.body.sMobile)
	{
		if (!validatePhone(req.body.sMobile))
		{
			return res.status(400).json({
				message: 'Invalid mobile number',
			});
		}
	}
	next();
};

validators.createBlog = async (req, res, next) =>
{
	if (!req.body.sTitle && !req.body.sDescription)
	{
		return res.status(400).json({
			message: 'title and description are required!',
		});
	}
	next();
};

module.exports = { validators };
