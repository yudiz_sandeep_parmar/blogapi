const userRouter = require('./userRouter');
const blogRouter = require('./blogRouter');
const bookRouter = require('./bookRouter');
const colorBookRouter = require('./colorBookRouter');
const textBookRouter = require('./textBookRouter');
const serverController = require('../controllers/serverController');
const express = require('express');

const router = new express.Router();

router.use(userRouter);
router.use(blogRouter);
router.use(bookRouter);
router.use(colorBookRouter);
router.use(textBookRouter);
router.get('/', serverController.server);
router.all('*', serverController.wildCard);
module.exports = router;
