const BookModel = require('../models/bookSchema');
const TextBookModel = require('../models/textBookSchema');
const ColorBookModel = require('../models/colorBookSchema');

const bookController = {};

bookController.create = async(req, res) => {
  try {
    const { id } = req.user;
    BookModel.create({
      oUserId: id,
      sTitle: req.body.sTitle

    }, function(err) {
      if (err) return err;
      return res.status(201).json({ message: 'book created ' });
    });
  } catch (e) {
    return res.status(500).json({ message: e.message });
  }
};
bookController.getBooks = async(req, res) => {
  try {
    const limit = 5;
    const page = req.query.pageNumber || 1;
    const data = await TextBookModel.find().populate('iBookId');

    console.log(data);
    const books = await BookModel
      .find({ })
      .skip(page * limit - limit)
      .limit(limit);
    return res.status(200).json(books);
  } catch (e) {
    return res.status(500).json({ message: e.message });
  }
};
module.exports = bookController;
