const express = require('express');
const router = new express.Router();

const auth = require('../middleware/auth');
const colorBookController = require('../controllers/colorBookController');

router.post(
  '/createColorBook',
  auth.userAuthorization,
  colorBookController.create
);

module.exports = router;
