const { gql } = require('apollo-server-express');

const typeDefs = gql`
  directive @upper on FIELD_DEFINITION
  union blogUnion = Blog | User
  scalar Email
  enum Role {
    ADMIN
    USER
    admin @deprecated(reason: "Use ADMIN instead")
  }

  interface Book {
    sTitle: String!
  }

  type Textbook implements Book {
    
    sTitle: String!
    sCourse: String
  }

  type ColoringBook implements Book {
    sTitle: String!
    sColor: String
  }

  type Blog  {
    oUserId: String
    sTitle: String
    sDescription: String
    sUserName: String!
    isPublished: Boolean
  }

  type User  {
    sUserName: String! @upper 
    sEmail: Email 
    sMobile: String 
    sGender: String
    sRole: [Role]
  }

  type Query {
    blogs:  [Blog]
    users:  [User] 
    search(args:String!): [blogUnion]
    books(args:String!): [Book!]
    searchUser(args: Email): [User]
  }
`;

module.exports = typeDefs;
