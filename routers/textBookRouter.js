const express = require('express');
const router = new express.Router();

const auth = require('../middleware/auth');
const textBookController = require('../controllers/textBookController');

router.post(
  '/createTextBook',
  auth.userAuthorization,
  textBookController.create
);

module.exports = router;
