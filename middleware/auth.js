const jwt = require('jsonwebtoken');

const userModel = require('../models/userSchema');

const jwtToken = process.env.TOKEN;

const auth = {};

auth.userAuthorization = async(req, res, next) => {
  try {
    const token = req.header('Authorization');
    const decoded = jwt.verify(token, jwtToken);
    const user = await userModel.findOne({
      _id: decoded._id,
      'aTokens.token': token
    });

    if (!user) {
      throw new Error({ message: 'Token expired!' });
    }

    req.token = token;
    req.user = user;
    next();
  } catch (e) {
    return res.status(401).send({ error: 'Please Authenticate!' });
  }
};

auth.adminAuthorization = (req, res, next) => {
  if (!req.user.sRole.includes('ADMIN')) return res.status(403).send({ error: 'Forbidden!' });
  next();
};

module.exports = auth;
