
const TextBookModel = require('../models/textBookSchema');

const textBookController = {};

textBookController.create = async(req, res) => {
  try {
    TextBookModel.create({
      iBookId: req.body.bookId,
      sTitle: req.body.sTitle,
      sCourse: req.body.sCourse
    }, function(err) {
      if (err) return err;
      return res.status(201).json({ message: 'textBook created ' });
    });
  } catch (e) {
    return res.status(500).json({ message: e.message });
  }
};

module.exports = textBookController;
